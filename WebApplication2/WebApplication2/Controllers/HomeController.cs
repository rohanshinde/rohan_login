﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using System.Web.Mvc;
using WebApplication2.Models;

namespace WebApplication2.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            LoginModel obj = new LoginModel();
            return View(obj);
        }
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> Index(LoginModel objuserlogin)
        {
            bool success = false;
            RequestData req;
            ResponseData res;

            string apikey = ConfigurationManager.AppSettings["ApiKey"];
            string secret = ConfigurationManager.AppSettings["Secret"];

            using (var client = new HttpClient())
            {
                 req = new RequestData
                {
                    ApiKey = apikey,
                    Secret = secret,
                };
               
                client.BaseAddress = new Uri("https://auth.aqumulate.com/token_handler/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                try
                {
                    HttpResponseMessage response = await client.PostAsJsonAsync("api/token", req);
                    if (response.IsSuccessStatusCode)
                    {
                        // decide if request succeed or not.
                        string result = response.Content.ReadAsStringAsync().Result;
                        res = JsonConvert.DeserializeObject<ResponseData>(result);


                        using (var client2 = new HttpClient())
                        {

                            client2.BaseAddress = new Uri("https://ceapiv2.aqumulate.com/");
                            client2.DefaultRequestHeaders.Accept.Clear();
                            client2.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", res.access_token);
                            client2.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                            try
                            {
                                HttpResponseMessage response2 = await client2.PostAsJsonAsync("api/Users/ClientSignOn", objuserlogin);
                                if (response2.IsSuccessStatusCode)
                                {
                                    success = true;
                                    // decide if request succeed or not.
                                    string result2 = response2.Content.ReadAsStringAsync().Result;
                                    LoginResponse LoginRes = JsonConvert.DeserializeObject<LoginResponse>(result2);
                                }
                            }
                            catch (Exception ex)
                            {

                            }
                        }
                    }
                }
                catch (Exception ex)
                {

                }
            }
            if (success)
            {
                return View("SuccessPage");
            }
            else
            {
                return View(objuserlogin);
            }
        }      
    }

    public class RequestData
    {
        public string ApiKey { get; set; }
        public string Secret { get; set; }       
    }

    public class ResponseData
    {
        public string access_token { get; set; }
        public string expires_in { get; set; }
        public string token_type { get; set; }
        public string scope { get; set; }
    }

    public class LoginResponse
    {
        public string SessionId { get; set; }
    
         public string UserCEID { get; set; }
    
         public string FirstName { get; set; }
   
         public string LastName { get; set; }
   
         public string SSN { get; set; }
                   
         public string Status { get; set; }
  
         public string ErrorMessage { get; set; }
   
         public string ExtendedErrorMessage { get; set; }
   
         public string ExtendedErrorCode { get; set; }
   
    }
}
