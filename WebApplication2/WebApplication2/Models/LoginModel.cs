﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication2.Models
{
    public class LoginModel
    {
        public string UserID { get; set; }
        public string Password { get; set; }

        public string SessionId { get; set; }
    }
}